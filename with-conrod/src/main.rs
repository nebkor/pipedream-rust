/*
The MIT License (MIT)

Copyright (c) 2014 PistonDevelopers

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#[macro_use]
extern crate conrod;

#[macro_use]
extern crate conrod_derive;

extern crate find_folder;

mod support;

mod widgets;

fn main() {
    use conrod::{self, widget, Colorable, Labelable, Positionable, Sizeable, Widget};
    use conrod::backend::glium::glium::{self, Surface};

    //use support;

    const WIDTH: u32 = 1200;
    const HEIGHT: u32 = 800;

    // Build the window.
    let mut events_loop = glium::glutin::EventsLoop::new();
    let window = glium::glutin::WindowBuilder::new()
        .with_title("Control Panel")
        .with_dimensions(WIDTH, HEIGHT);
    let context = glium::glutin::ContextBuilder::new()
        .with_vsync(false)
        .with_multisampling(4);
    let display = glium::Display::new(window, context, &events_loop).unwrap();

    // construct our `Ui`.
    let mut ui = conrod::UiBuilder::new([WIDTH as f64, HEIGHT as f64]).build();

    // The `widget_ids` macro is a easy, safe way of generating a type for producing `widget::Id`s.
    widget_ids! {
        struct Ids {
            // An ID for the background widget, upon which we'll place our custom button.
            canvas,
            // The WidgetId we'll use to plug our widget into the `Ui`.
            button_add,
            button_kill,
            circle_button,
            child[],
        }
    }
    let mut ids = Ids::new(ui.widget_id_generator());

    // Add a `Font` to the `Ui`'s `font::Map` from file.
    let assets = find_folder::Search::KidsThenParents(3, 5)
        .for_folder("assets")
        .unwrap();
    let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
    let regular = ui.fonts.insert_from_file(font_path).unwrap();

    // A type used for converting `conrod::render::Primitives` into `Command`s that can be used
    // for drawing to the glium `Surface`.
    let mut renderer = conrod::backend::glium::Renderer::new(&display).unwrap();

    // The image map describing each of our widget->image mappings (in our case, none).
    let image_map = conrod::image::Map::<glium::texture::Texture2d>::new();

    let mut kidcount = 0;
    #[allow(unused_assignments)]
    let mut parent: Option<conrod::widget::Id> = None;

    // set this after iterating over children to the last one
    // so that we know which child can kill itself on click.
    let mut last_child: Option<conrod::widget::Id> = None;

    // Poll events from the window.
    let mut event_loop = support::EventLoop::new();
    'main: loop {
        // Handle all events.
        for event in event_loop.next(&mut events_loop) {
            // Use the `winit` backend feature to convert the winit event to a conrod one.
            if let Some(input) = conrod::backend::winit::convert_event(event.clone(), &display) {
                // only update when there's something besides a mouse-move; cuts down on CPU use
                match input {
                    conrod::event::Input::Motion(_) => {
                        ui.handle_event(input);
                    }
                    _ => {
                        ui.handle_event(input);
                        event_loop.needs_update();
                    }
                }
            }

            match event {
                glium::glutin::Event::WindowEvent { event, .. } => match event {
                    // Break from the loop upon `Escape`.
                    glium::glutin::WindowEvent::Closed
                    | glium::glutin::WindowEvent::KeyboardInput {
                        input:
                            glium::glutin::KeyboardInput {
                                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    }
                    | glium::glutin::WindowEvent::KeyboardInput {
                        input:
                            glium::glutin::KeyboardInput {
                                virtual_keycode: Some(glium::glutin::VirtualKeyCode::Q),
                                ..
                            },
                        ..
                    } => break 'main,
                    _ => (),
                },
                _ => (),
            }
        }

        // Instantiate the widgets.
        {
            let ui = &mut ui.set_widgets();

            // Sets a color to clear the background with before the Ui draws our widget.
            widget::Canvas::new()
                .color(conrod::color::BLUE)
                .set(ids.canvas, ui);

            // lazily generates widget ids for the child canvases, later
            // gotten by iterating over ids.child
            ids.child.resize(kidcount, &mut ui.widget_id_generator());

            let mut prev_id: Option<conrod::widget::Id> = None;

            for id in ids.child.iter() {
                // println!("child: {:?}", id);
                if let Some(pid) = prev_id {
                    parent = Some(pid);
                } else {
                    parent = Some(ids.button_add);
                }

                prev_id = Some(id.clone());

                for _click in widget::Button::new()
                    .color(conrod::color::CHARCOAL)
                    .w_h(128.0, 79.0)
                    .floating(true)
                    .label_font_id(regular)
                    .label((format!("{:?}", id)).as_str())
                    .right_from(parent.unwrap(), 20.0)
                    .set(*id, ui)
                {
                    if prev_id == last_child {
                        println!("{:?} died", id);
                        kidcount -= 1;
                    } else {
                        println!("{:?} too young to die", id);
                    }
                }
            }

            // prev_id is now the last child, we're out of the for loop
            last_child = prev_id;

            // Instantiate of our custom widget.
            for _click in widget::Button::new()
                .color(conrod::color::DARK_ORANGE)
                .top_left_of(ids.canvas)
                .w_h(256.0, 158.0)
                .label_font_id(regular)
                .label_color(conrod::color::WHITE)
                .label("Spawn child")
                // Add the widget to the conrod::Ui. This schedules the widget it to be
                // drawn when we call Ui::draw.
                .set(ids.button_add, ui)
            {
                println!("Spawn!");
                kidcount += 1;
            }

            for _click in widget::Button::new()
                .color(conrod::color::DARK_ORANGE)
                .down_from(ids.button_add, 20.0)
                .w_h(256.0, 158.0)
                .label_font_id(regular)
                .label_color(conrod::color::LIGHT_CHARCOAL)
                .label("INFANTICIDE")
                // Add the widget to the conrod::Ui. This schedules the widget it to be
                // drawn when we call Ui::draw.
                .set(ids.button_kill, ui)
            {
                println!("kill!");
                if kidcount > 0 {
                    kidcount -= 1;
                }
            }

            for _click in widgets::CircularButton::new()
                .color(conrod::color::rgb(0.0, 0.3, 0.1))
                .down_from(ids.button_kill, 10.0)
                .w_h(256.0, 256.0)
                .label_font_id(regular)
                .label("custom button")
                .set(ids.circle_button, ui)
            {
                println!("circles, brah.");
            }
        }

        // Render the `Ui` and then display it on the screen.
        if let Some(primitives) = ui.draw_if_changed() {
            renderer.fill(&display, primitives, &image_map);
            let mut target = display.draw();
            target.clear_color(0.0, 0.0, 0.0, 1.0);
            renderer.draw(&display, &mut target, &image_map).unwrap();
            target.finish().unwrap();
        }
    }
}
